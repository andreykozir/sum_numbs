<?php

function generateJsonResponse(int $sum, string $message, string $statusText, int $status)
{
    return json_encode([
        'sum' => $sum,
        'message' => $message,
        'status' => $statusText,
    ], $status);
}