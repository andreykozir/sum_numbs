<?php


if(!file_exists('./functions.php') || !file_exists('./config.php')) {
    die('functions.php or config.php is not exists');
}

require_once './config.php';
require_once './functions.php';

//Не можем выполнить программу т.к. не заполненны обязательные поля
if (!isset($_POST['number1']) || !isset($_POST['number2'])) {
    echo generateJsonResponse(0, 'Set numbers', 'error', 422);
    exit;
}

$number1 = $_POST['number1'];
$number2 = $_POST['number2'];

$status = 200;
$statusText = '';
$message = '';
$sum = 0;

//проверяем заполненность переменных
if ($number1 === '' || $number2 === '') {
    $status = 422;
    $statusText = 'error';
    $message = 'Enter both digits';
} else {
    $sum = (int)$number1 + (int)$number2;
    $message = 'Sum of numbers: ' . $sum;
    $statusText = 'success';
}

echo generateJsonResponse($sum, $message, $statusText, $status);
