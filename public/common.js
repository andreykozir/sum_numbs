$(function() {
    $('#calculator-form').submit(function(event) {
        //отменяем стандартное выполнение отправки формы. Будем использовать своё собственное.
        event.preventDefault();

        //получаем URL используя элемент формы action
        let url = $(this).prop('action');

        let calculatorResponse = $('#calculator-response');
        $.ajax({
            url: url,
            //создаем массив данных для отправки на сервер
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                //анализируем ответ
                if(data.status === 'success') {
                    calculatorResponse.removeClass('error').text(data.message);
                } else {
                    calculatorResponse.addClass('error').removeClass('success').text('Error: ' + data.message);
                }
            },
            error: function(e) {
                calculatorResponse.addClass('error').text('Error has occurred, try to submit your request again');
            }
        });

    });
});
